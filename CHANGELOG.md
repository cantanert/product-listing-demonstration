# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/cantanert/product-listing-demonstration/compare/v1.1.0...v1.2.0) (2023-11-13)


### Bug Fixes

* number input leading "0" fixed in statistics page ([f61a4ed](https://gitlab.com/cantanert/product-listing-demonstration/commit/f61a4ed84aa7b3c762dfdfac5bf6fd45a21aeaae))


### Documentation Changes

* "Semantic Versioning" field addition to README.md file ([6b14c9c](https://gitlab.com/cantanert/product-listing-demonstration/commit/6b14c9c741dd65c6c638ed80a2dd49503f223a05))
* README.md editing ([25874f9](https://gitlab.com/cantanert/product-listing-demonstration/commit/25874f9080d7abc4f670cdfedd47627f2c10d781))

## [1.1.0](https://github.com/cantanert/product-listing-demonstration/compare/v1.0.0...v1.1.0) (2023-11-12)


### Documentation Changes

* README.md editing ([f5295ef](https://github.com/cantanert/product-listing-demonstration/commit/f5295eff5eb5c17c704188f6ccd8d51bbbb9968c))

## 1.0.0 (2023-11-12)


### Features

* "classnames" and "antd" libs addition, layout implementation with navbar and related page initialization ([d03b133](https://github.com/cantanert/product-listing-demonstration/commit/d03b1336a9db11e24baaff6480f0dd8843693283))
* GET /search API implementation with sorting and filtering ability ([2c289f9](https://github.com/cantanert/product-listing-demonstration/commit/2c289f9b2d0aa64a5ed1b166ba3dc3ed746587e6))
* GET /statistics API implementation with "withMostSizeOptions", "withLowestAveragePriceForSize" and "withMostProductLtPrice" parameters ([c91289d](https://github.com/cantanert/product-listing-demonstration/commit/c91289de51de7b5d0ba37e9aa4a5dee845c8a804))
* implementation of filtering and sorting action connection between client and API ([1739cd5](https://github.com/cantanert/product-listing-demonstration/commit/1739cd5e3ac02ba1c720b62a7da32e480e2f21c3))
* implementation of statistics question&answer action connection between client and API ([98c81d6](https://github.com/cantanert/product-listing-demonstration/commit/98c81d6467015a1eec0aa64d58493aa5257aaf68))
* metadata and favicon changes ([1f565bd](https://github.com/cantanert/product-listing-demonstration/commit/1f565bdd0effed89c1944f9ad22a607793864fc9))
* productList design layout responsive implementation, unnecessary antd dependency deletion ([4e5a3b8](https://github.com/cantanert/product-listing-demonstration/commit/4e5a3b8290c12771042560015da00edb86d00980))
* unit test implementations ([2db6499](https://github.com/cantanert/product-listing-demonstration/commit/2db6499e2ed45ddaaf24707f79b2bdec59fda582))


### Code Quality & Formatting

* eslint airbnb plugin and prettier config additions, project-wide code formatting ([90c3c2e](https://github.com/cantanert/product-listing-demonstration/commit/90c3c2e93a5683641feb0579daa08b6d8645c79c))


### Configurations

* pre-commit git hook tsc addition, commitlint "header-max-length" rule overriding ([c436b43](https://github.com/cantanert/product-listing-demonstration/commit/c436b43dc2dfcda8b728d0b059a51793c9e32bd7))
* project setup initialization ([a9df667](https://github.com/cantanert/product-listing-demonstration/commit/a9df667d3ea9a934d1b8f81c959d0d40fa732262))


### Documentation Changes

* CHANGELOG.md generator and semantic versiner integration ([3aa7508](https://github.com/cantanert/product-listing-demonstration/commit/3aa75083c7c0f9cb7a539235c8ddb6513dc70145))
