import { API_PATHS, BASE_URL } from '@/src/utils/urls';
import { EStatisticsUrlParameters } from '@/src/utils/constants';

export const fetchStatisticsByQuestions = async (
  question: EStatisticsUrlParameters,
  value?: string | number,
) => {
  try {
    const res = await fetch(
      `${BASE_URL}${API_PATHS.STATISTICS}?${question}=${value ?? ''}`,
    );
    return await res.json();
  } catch (error) {
    return null;
  }
};
