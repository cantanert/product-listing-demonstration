import { API_PATHS, BASE_URL } from '@/src/utils/urls';

export const fetchProducts = async (searchParams?: string) => {
  try {
    const res = await fetch(
      `${BASE_URL}${API_PATHS.SEARCH}?${searchParams ?? ''}`,
    );
    return await res.json();
  } catch (e) {
    console.log(e);
    return null;
  }
};
