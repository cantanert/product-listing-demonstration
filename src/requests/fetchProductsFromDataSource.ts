import { API_PATHS } from '@/src/utils/urls';

export const fetchProductsFromDataSource = async () => {
  const res = await fetch(API_PATHS.DATA_SOURCE_CDN_URL);
  return res.json();
};
