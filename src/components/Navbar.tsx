'use client';

import Link from 'next/link';
import { RouteLinksGenerator } from '@/src/components/RouteLinksGenerator';
import { ROUTES } from '@/src/utils/routes';
import React from 'react';
import { usePathname } from 'next/navigation';
import { NAVBAR_TEST_ID } from '@/src/utils/testids';

export const Navbar = () => {
  const pathname = usePathname();
  return (
    <div
      id="Navbar"
      className="w-full flex flex-col items-center justify-center"
    >
      <div className="bg-black text-white p-3 desktop:p-5 flex justify-center items-center w-full">
        <div className="max-w-screen-widescreen w-full flex justify-between items-center">
          <Link
            href="/"
            className="font-extrabold italic"
            data-testid={NAVBAR_TEST_ID.LOGO}
          >
            LOREMIPSUM
          </Link>
          <div className="flex gap-5 desktop:gap-10">
            <RouteLinksGenerator />
          </div>
        </div>
      </div>
      <h1
        className="max-w-screen-widescreen w-full pt-5 pb-2 pl-2 pr-2 desktop:pt-10 desktop:pb-5 widescreen:pl-0 widescreen:pr-0 text-xl desktop:text-3xl font-extrabold"
        data-testid={NAVBAR_TEST_ID.PAGE_HEADER}
      >
        {ROUTES.find((route) => route.path === pathname)?.name}
      </h1>
    </div>
  );
};
