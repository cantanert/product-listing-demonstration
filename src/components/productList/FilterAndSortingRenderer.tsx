import React, { HTMLAttributes } from 'react';
import classNames from 'classnames';
import { FILTER_AND_SORTING_TEST_ID } from '@/src/utils/testids';

interface IFilterAndSortingRendererProps {
  title: string;
  data: string[];
  appliedItems: string[];
  clickHandler: (item: string) => void;
}

export const FilterAndSortingRenderer = (
  props: IFilterAndSortingRendererProps & HTMLAttributes<HTMLElement>,
) => {
  const { title, data, appliedItems, clickHandler, className } = props;
  return (
    <div
      className={classNames({
        'flex flex-col w-full desktop:w-2/12': true,
        [className as string]: !!className,
      })}
    >
      <h1
        className="font-extrabold mb-5"
        data-testid={FILTER_AND_SORTING_TEST_ID.TITLE}
      >
        {title}
      </h1>
      <div className="flex flex-wrap gap-2">
        {data.map((item) => (
          <button
            type="button"
            className={classNames({
              'w-fit border rounded-lg text-xs pt-1 pb-1 pl-2 pr-2': true,
              'bg-gray-100': !appliedItems?.includes(item),
              'bg-black text-white': appliedItems?.includes(item),
            })}
            key={item}
            onClick={() => clickHandler(item)}
            data-testid={FILTER_AND_SORTING_TEST_ID.FILTER_ITEM}
          >
            {item}
          </button>
        ))}
      </div>
    </div>
  );
};
