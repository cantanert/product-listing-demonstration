'use client';

import { FilterAndSortingRenderer } from '@/src/components/productList/FilterAndSortingRenderer';
import { useEffect, useState } from 'react';
import { useRouter, useSearchParams } from 'next/navigation';
import { ESearchUrlParameters, ESortingOptions } from '@/src/utils/constants';
import { APP_ROUTES } from '@/src/utils/routes';

export const SortingByPrice = ({ data }: { data: string[] }) => {
  const router = useRouter();
  const params = useSearchParams();
  const searchParams = new URLSearchParams(params);

  const sortByParam = params.get(ESearchUrlParameters.SORT_BY);

  let sortOption = null;

  if (sortByParam && Object.keys(ESortingOptions).includes(sortByParam)) {
    sortOption = sortByParam;
  }

  const [appliedSorting, setAppliedSorting] = useState<string | null>(
    sortOption,
  );
  const [isFirstRender, setIsFirstRender] = useState<boolean>(true);

  useEffect(() => {
    if (isFirstRender) {
      setIsFirstRender(false);
    } else if (appliedSorting) {
      searchParams.set(ESearchUrlParameters.SORT_BY, appliedSorting);
    } else {
      searchParams.delete(ESearchUrlParameters.SORT_BY);
    }
    router.push(`${APP_ROUTES.PRODUCT_LIST}?${searchParams.toString()}`);
  }, [appliedSorting]);

  const sortingClickHandler = (item: string) => {
    appliedSorting === item ? setAppliedSorting(null) : setAppliedSorting(item);
  };

  return (
    <FilterAndSortingRenderer
      title="Sorting"
      className="order-2 desktop:order-3"
      data={data}
      appliedItems={appliedSorting ? [appliedSorting] : []}
      clickHandler={sortingClickHandler}
    />
  );
};
