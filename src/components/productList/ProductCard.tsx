import { IProduct } from '@/src/models/product';
import { HTMLAttributes } from 'react';
import classNames from 'classnames';
import Image from 'next/image';
import Link from 'next/link';
import { PRODUCT_CARD_TEST_ID } from '@/src/utils/testids';

interface IProductCardProps {
  productData: IProduct;
}

export const ProductCard = (
  props: IProductCardProps & HTMLAttributes<HTMLElement>,
) => {
  const { productData } = props;
  return (
    <Link
      className={classNames({
        [props?.className as string]: true,
        'p-3 rounded-lg desktop:hover:shadow-2xl': true,
      })}
      href={productData?.url}
      data-testid={PRODUCT_CARD_TEST_ID.PRODUCT_CARD_WRAPPER}
    >
      <div id="ProductCard" className="flex flex-col h-full">
        <Image
          src={productData?.images[0]}
          alt={productData?.description}
          width={200}
          height={200}
          data-testid={PRODUCT_CARD_TEST_ID.PRODUCT_IMAGE}
        />
        <div className="flex flex-1 items-start">
          <div className="flex items-center">
            {!!productData?.priceR && (
              <span
                className="line-through text-xs mr-1"
                data-testid={PRODUCT_CARD_TEST_ID.ORIGINAL_PRICE}
              >
                {productData?.priceO}€
              </span>
            )}
            <span
              className={classNames({
                'font-bold text-l': true,
                'text-red': productData?.priceR,
              })}
              data-testid={
                productData?.priceR
                  ? PRODUCT_CARD_TEST_ID.REDUCED_PRICE
                  : PRODUCT_CARD_TEST_ID.ORIGINAL_PRICE
              }
            >
              {productData?.priceR ?? productData.priceO}€
            </span>
          </div>
        </div>
        <div className="flex flex-col flex-1">
          <div>
            <p
              className="font-extrabold text-sm"
              data-testid={PRODUCT_CARD_TEST_ID.BRAND_NAME}
            >
              {productData?.brand}
            </p>
            <p
              className="text-xs"
              data-testid={PRODUCT_CARD_TEST_ID.BRAND_DESC}
            >
              {productData?.description}
            </p>
          </div>
        </div>
      </div>
    </Link>
  );
};
