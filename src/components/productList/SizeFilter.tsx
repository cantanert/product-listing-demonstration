'use client';

import { FilterAndSortingRenderer } from '@/src/components/productList/FilterAndSortingRenderer';
import { useEffect, useState } from 'react';
import { useRouter, useSearchParams } from 'next/navigation';
import { EDelimiter, ESearchUrlParameters } from '@/src/utils/constants';
import { APP_ROUTES } from '@/src/utils/routes';

export const SizeFilter = ({ data }: { data: string[] }) => {
  const router = useRouter();
  const params = useSearchParams();
  const searchParams = new URLSearchParams(params);

  const sizeParam = params.get(ESearchUrlParameters.SIZE);

  let parsedSizeParams: string[] = [];

  if (sizeParam) {
    parsedSizeParams = sizeParam.toUpperCase().split(EDelimiter.COMMA);
  }

  const [appliedSizes, setAppliedSizes] = useState<string[]>([
    ...parsedSizeParams,
  ]);
  const [isFirstRender, setIsFirstRender] = useState<boolean>(true);

  useEffect(() => {
    if (isFirstRender) {
      setIsFirstRender(false);
    } else {
      if (appliedSizes.length) {
        searchParams.set(
          ESearchUrlParameters.SIZE,
          appliedSizes.join(EDelimiter.COMMA),
        );
      } else {
        searchParams.delete(ESearchUrlParameters.SIZE);
      }
      router.push(`${APP_ROUTES.PRODUCT_LIST}?${searchParams.toString()}`);
    }
  }, [appliedSizes]);

  const filterClickHandler = (item: string) => {
    appliedSizes.includes(item)
      ? setAppliedSizes(appliedSizes.filter((size) => size !== item))
      : setAppliedSizes([...appliedSizes, item]);
  };

  return (
    <FilterAndSortingRenderer
      title="Filters"
      className="order-1"
      data={data}
      appliedItems={appliedSizes}
      clickHandler={filterClickHandler}
    />
  );
};
