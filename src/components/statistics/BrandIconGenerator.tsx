import { BRAND_ICON_TEST_ID } from '@/src/utils/testids';

export const BrandIconGenerator = ({ brandName }: { brandName: string }) => {
  const brandAbbreviation = brandName
    .toUpperCase()
    .split(' ')
    .slice(0, 2)
    .map((item) => item[0])
    .join('');

  return (
    <p
      key={brandName}
      className="w-7 h-7 flex justify-center items-center p-4 bg-red text-white rounded-md font-extrabold"
      data-testid={BRAND_ICON_TEST_ID.ABBREVIATION}
    >
      {brandAbbreviation}
    </p>
  );
};
