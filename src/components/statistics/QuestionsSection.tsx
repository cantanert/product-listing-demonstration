'use client';

import { useEffect, useState } from 'react';
import { UseDebounce } from '@/src/hooks/UseDebounce';
import { fetchStatisticsByQuestions } from '@/src/requests/fetchStatisticsByQuestions';
import { EStatisticsUrlParameters } from '@/src/utils/constants';
import { QUESTIONS_SECTION_TEST_ID } from '@/src/utils/testids';
import { BrandWithMostProductQuestion } from '@/src/components/statistics/questions/BrandWithMostProductQuestion';
import { BrandWithMostSizesQuestion } from '@/src/components/statistics/questions/BrandWithMostSizesQuestion';
import { BrandWithLowestAveragePriceQuestion } from '@/src/components/statistics/questions/BrandWithLowestAveragePriceQuestion';

export const INITIAL_BASE_PRICE = 40;
export const INITIAL_BASE_SIZE = '32';

interface IQuestionSectionPropsType {
  sizeFilterOptions: string[];
  initialBrandWithMostProductLt: string[];
  initialBrandWithMostSize: string[];
  initialBrandWithLowestAveragePriceForSize: string[];
}

export const QuestionsSection = ({
  sizeFilterOptions,
  initialBrandWithMostProductLt,
  initialBrandWithMostSize,
  initialBrandWithLowestAveragePriceForSize,
}: IQuestionSectionPropsType) => {
  const [price, setPrice] = useState<number>(INITIAL_BASE_PRICE);
  const [size, setSize] = useState<string>(INITIAL_BASE_SIZE);

  const [brandWithMostProductLt, setBrandWithMostProductLt] = useState<
    string[]
  >(initialBrandWithMostProductLt ?? []);

  const [
    brandWithLowestAveragePriceForSize,
    setBrandWithLowestAveragePriceForSize,
  ] = useState<string[]>(initialBrandWithLowestAveragePriceForSize ?? []);

  const [isInitialValueDebounce, setIsInitialValueDebounce] =
    useState<boolean>(true);

  const debounce = UseDebounce(price, 1000);

  useEffect(() => {
    if (isInitialValueDebounce) {
      setIsInitialValueDebounce(false);
    } else {
      fetchStatisticsByQuestions(
        EStatisticsUrlParameters.WITH_MOST_PRODUCT_LT_PRICE,
        price,
      ).then(({ brands }) => {
        setBrandWithMostProductLt(brands);
      });
    }
  }, [debounce]);

  const sizeOnChangeHandler = async (newSize: string) => {
    setSize(newSize);
    fetchStatisticsByQuestions(
      EStatisticsUrlParameters.LOWEST_AVERAGE_PRICE_FOR_SIZE,
      newSize,
    ).then(({ brands }) => {
      setBrandWithLowestAveragePriceForSize(brands);
    });
  };

  const priceOnChangeHandler = (newPrice: string) => {
    setPrice(+newPrice ?? 0);
  }

  return (
    <div className="flex flex-col gap-10 mt-3 desktop:mt-10" data-testid="sdsd">
      <div
        className="mb-5 flex flex-col gap-2"
        data-testid={QUESTIONS_SECTION_TEST_ID.QUESTION}
      >
        <BrandWithMostProductQuestion
          price={price}
          priceChangeHandler={priceOnChangeHandler}
          brands={brandWithMostProductLt}
        />
      </div>
      <div
        className="mb-5 flex flex-col gap-2"
        data-testid={QUESTIONS_SECTION_TEST_ID.QUESTION}
      >
        <BrandWithMostSizesQuestion brands={initialBrandWithMostSize} />
      </div>
      <div
        className="mb-5 flex flex-col gap-2"
        data-testid={QUESTIONS_SECTION_TEST_ID.QUESTION}
      >
        <BrandWithLowestAveragePriceQuestion
          sizeFilterOptions={sizeFilterOptions}
          brands={brandWithLowestAveragePriceForSize}
          size={size}
          sizeChangeHandler={sizeOnChangeHandler}
        />
      </div>
    </div>
  );
};
