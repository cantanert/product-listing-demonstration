import { BrandIconGenerator } from '@/src/components/statistics/BrandIconGenerator';
import { BRAND_CARD_TEST_ID } from '@/src/utils/testids';

export const BrandCardGenerator = ({ brands }: { brands: string[] }) => (
  <div
    className="flex flex-col gap-2"
    data-testid={BRAND_CARD_TEST_ID.BRAND_CARD}
  >
    {brands?.map((brand) =>
      brand || brand !== '' ? (
        <div key={brand} className="flex items-center gap-1">
          <BrandIconGenerator brandName={brand} />
          <p
            className="text-xl font-extrabold"
            data-testid={BRAND_CARD_TEST_ID.BRAND_NAME}
          >
            {brand}
          </p>
        </div>
      ) : (
        '-'
      ),
    )}
  </div>
);
