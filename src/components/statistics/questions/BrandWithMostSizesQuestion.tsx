import { QUESTIONS_SECTION_TEST_ID } from '@/src/utils/testids';
import { BrandCardGenerator } from '@/src/components/statistics/BrandCardGenerator';

export const BrandWithMostSizesQuestion = ({
  brands,
}: {
  brands: string[];
}) => (
  <div>
    <p className="text-xl">
      Which brand offers the largest selection of sizes to the customer?
    </p>
    <div data-testid={QUESTIONS_SECTION_TEST_ID.BRAND_WITH_MOST_SIZE}>
      <BrandCardGenerator brands={brands} />
    </div>
  </div>
);
