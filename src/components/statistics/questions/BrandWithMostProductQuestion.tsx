import { QUESTIONS_SECTION_TEST_ID } from '@/src/utils/testids';
import { BrandCardGenerator } from '@/src/components/statistics/BrandCardGenerator';

export const BrandWithMostProductQuestion = ({
  price,
  priceChangeHandler,
  brands,
}: {
  price: number;
  priceChangeHandler: (val: string) => void;
  brands: string[];
}) => (
  <>
    <p className="text-xl">
      Which brand has the most products that cost less than
      <input
        type="number"
        min={0}
        id="price"
        name="price"
        className="w-[70px] h-[25px] border border-black rounded-full pl-2 pr-2 ml-1 mr-1 outline-0 w-15 text-sm text-center"
        onChange={(e) => priceChangeHandler(e.target.value)}
        value={Number(price).toString()}
        data-testid={QUESTIONS_SECTION_TEST_ID.BRAND_WITH_MOST_PRODUCT_LT_INPUT}
      />
      EUR?
    </p>
    <div data-testid={QUESTIONS_SECTION_TEST_ID.BRAND_WITH_MOST_PRODUCT_LT}>
      <BrandCardGenerator brands={brands} />
    </div>
  </>
);
