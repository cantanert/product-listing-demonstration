import { QUESTIONS_SECTION_TEST_ID } from '@/src/utils/testids';
import { BrandCardGenerator } from '@/src/components/statistics/BrandCardGenerator';

export const BrandWithLowestAveragePriceQuestion = ({
  sizeFilterOptions,
  size,
  sizeChangeHandler,
  brands,
}: {
  sizeFilterOptions: string[];
  size: string;
  sizeChangeHandler: (val: string) => void;
  brands: string[];
}) => (
  <>
    <p className="text-xl">
      Which brand offers the lowest average price for customers wearing size
      <select
        name="sizes"
        id="sizes"
        className="border border-black rounded-full m-1 outline-0 cursor-pointer text-sm w-15 h-[25px] text-center"
        onChange={(e) => sizeChangeHandler(e.target.value)}
        value={size}
        data-testid={
          QUESTIONS_SECTION_TEST_ID.BRAND_WITH_LOWEST_AVERAGE_PRICE_INPUT
        }
      >
        {sizeFilterOptions.map((sizeFilter: string) => (
          <option key={sizeFilter} value={sizeFilter}>
            {sizeFilter}
          </option>
        ))}
      </select>
      ?
    </p>
    <div
      data-testid={QUESTIONS_SECTION_TEST_ID.BRAND_WITH_LOWEST_AVERAGE_PRICE}
    >
      <BrandCardGenerator brands={brands} />
    </div>
  </>
);
