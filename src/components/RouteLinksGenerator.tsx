'use client';

import { ROUTES } from '@/src/utils/routes';
import Link from 'next/link';
import { usePathname } from 'next/navigation';

export const RouteLinksGenerator = () => {
  const pathname = usePathname();

  return ROUTES.map((route) => (
    <Link
      key={route.path}
      href={route.path}
      className={`${
        pathname === route.path ? 'font-extrabold italic' : 'font-extralight'
      }`}
      data-testid={`${route.name}-route-link`}
    >
      {route.name}
    </Link>
  ));
};
