export const API_PATHS = {
  DATA_SOURCE_CDN_URL:
    'https://s3-eu-west-1.amazonaws.com/fid-recruiting/fid-task-4-ffront-products.json',
  SEARCH: '/api/search',
  STATISTICS: '/api/statistics',
};

export const BASE_URL = 'http://localhost:3000';
