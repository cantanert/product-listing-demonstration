import { IProduct } from '@/src/models/product';

export const groupByProperty = (
  sourceArray: IProduct[],
  property: keyof IProduct,
) =>
  sourceArray.reduce((acc: { [k: string]: any[] }, cur: any) => {
    acc[cur[property]] = acc[cur[property]] ?? [];
    acc[cur[property]].push(cur);
    return acc;
  }, {});
