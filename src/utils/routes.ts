export const APP_ROUTES = {
  PRODUCT_LIST: '/productList',
  STATISTICS: '/statistics',
};
export const ROUTES = [
  {
    path: APP_ROUTES.PRODUCT_LIST,
    name: 'Products',
  },
  {
    path: APP_ROUTES.STATISTICS,
    name: 'Statistics',
  },
];
