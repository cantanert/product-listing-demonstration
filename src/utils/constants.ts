export enum ESearchUrlParameters {
  SORT_BY = 'sortBy',
  SIZE = 'size',
}

export enum EStatisticsUrlParameters {
  WITH_MOST_PRODUCT_LT_PRICE = 'withMostProductLtPrice',
  WITH_MOST_SIZE_OPTIONS = 'withMostSizeOptions',
  LOWEST_AVERAGE_PRICE_FOR_SIZE = 'withLowestAveragePriceForSize',
}

export enum ESortingOptions {
  PRICE_DESC = 'PRICE_DESC',
  PRICE_ASC = 'PRICE_ASC',
}

export enum EStatus {
  ERROR = 'Error',
}

export enum EDelimiter {
  COMMA = ',',
}

export enum EInterpolationString {
  PARAMETER = '[[parameter]]',
}
