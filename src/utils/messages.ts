import { EInterpolationString } from '@/src/utils/constants';

export const MESSAGE = {
  INTERNAL_SERVER_ERROR: 'Internal Server Error.',
  INVALID_PARAMETER: `Invalid ${EInterpolationString.PARAMETER} value.`,
  UNDEFINED_PARAMETER_VALUE: `Undefined ${EInterpolationString.PARAMETER} value.`,
  UNSPECIFIED_PARAMETER: `Unspecified parameter. Possible parameters: ${EInterpolationString.PARAMETER}`,
};
