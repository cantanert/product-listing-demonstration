export const PRODUCT_CARD_TEST_ID = {
  PRODUCT_CARD_WRAPPER: 'ProductCard_product-card-wrapper',
  PRODUCT_IMAGE: 'ProductCard_product-image',
  BRAND_NAME: 'ProductCard_brand-name',
  BRAND_DESC: 'ProductCard_brand-desc',
  ORIGINAL_PRICE: 'ProductCard_original-price',
  REDUCED_PRICE: 'ProductCard_reduced-price',
};

export const FILTER_AND_SORTING_TEST_ID = {
  TITLE: 'FilterAndSortingRenderer_title',
  FILTER_ITEM: 'FilterAndSortingRenderer_filter-item',
};

export const BRAND_CARD_TEST_ID = {
  BRAND_NAME: 'BrandCardGenerator_brand-name',
  BRAND_CARD: 'BrandCardGenerator_brand-card',
};

export const BRAND_ICON_TEST_ID = {
  ABBREVIATION: 'BrandIconGenerator_brand-name-abbreviation',
};

export const NAVBAR_TEST_ID = {
  LOGO: 'Navbar_logo',
  PAGE_HEADER: 'Navbar_page-header',
};

export const QUESTIONS_SECTION_TEST_ID = {
  QUESTION: 'QuestionsSection_question',
  BRAND_WITH_MOST_PRODUCT_LT: 'QuestionsSection_brand-with-most-product-lt',
  BRAND_WITH_MOST_PRODUCT_LT_INPUT:
    'QuestionsSection_brand-with-most-product-lt-input',
  BRAND_WITH_MOST_SIZE: 'QuestionsSection_brand-with-most-size',
  BRAND_WITH_LOWEST_AVERAGE_PRICE_INPUT:
    'QuestionsSection_brand-with-lowest-average-price-input',
  BRAND_WITH_LOWEST_AVERAGE_PRICE:
    'QuestionsSection_brand-with-lowest-average-price',
};
