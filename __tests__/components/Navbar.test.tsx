import { render, screen } from '@testing-library/react';
import { Navbar } from '@/src/components/Navbar';
import { NAVBAR_TEST_ID } from '@/src/utils/testids';

const mockUsePathname = jest.fn();

jest.mock('next/navigation', () => ({
  usePathname() {
    return mockUsePathname();
  },
}));

describe('Navbar.tsx', () => {
  it('should have "LOREMIPSUM" brand logo text', async () => {
    render(<Navbar />);
    const brandLogo = screen.getByTestId(NAVBAR_TEST_ID.LOGO);
    expect(brandLogo).toContainHTML('LOREMIPSUM');
  });

  it('should not render an initial <h1 /> tag', () => {
    render(<Navbar />);
    const pageHeader = screen.getByTestId(NAVBAR_TEST_ID.PAGE_HEADER);
    expect(pageHeader).toContainHTML('');
  });

  it('should render exact router Name as within <h1 /> tag', () => {
    mockUsePathname.mockImplementation(() => '/productList');
    render(<Navbar />);
    const pageHeader = screen.getByTestId(NAVBAR_TEST_ID.PAGE_HEADER);
    expect(pageHeader).toContainHTML('Products');
  });
});
