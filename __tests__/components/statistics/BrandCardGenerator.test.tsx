import { render, screen } from '@testing-library/react';
import { BrandCardGenerator } from '@/src/components/statistics/BrandCardGenerator';
import { BRAND_CARD_TEST_ID } from '@/src/utils/testids';

const mockedBrands = ['Fever London'];

describe('BrandCardGenerator.tsx', () => {
  it('should render all the given brand names as "extrabold" texts', async () => {
    render(<BrandCardGenerator brands={mockedBrands} />);
    mockedBrands.forEach((brand) => {
      const brandNameElements = screen.getAllByTestId(
        BRAND_CARD_TEST_ID.BRAND_NAME,
      );
      brandNameElements.forEach((brandName) => {
        expect(brandName).toContainHTML(brand);
        expect(brandName).toHaveClass('font-extrabold');
      });
    });
  });

  it('should not render any brand name', () => {
    render(<BrandCardGenerator brands={[]} />);
    const brandNameElement = screen.queryByTestId(
      BRAND_CARD_TEST_ID.BRAND_NAME,
    );
    expect(brandNameElement).not.toBeInTheDocument();
  });

  it('should render "-" if there is no given brand exist', () => {
    render(<BrandCardGenerator brands={[]} />);
    const brandElement = screen.getByTestId(BRAND_CARD_TEST_ID.BRAND_CARD);
    expect(brandElement).toContainHTML('-');
  });
});
