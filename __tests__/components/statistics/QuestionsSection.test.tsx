import {
  INITIAL_BASE_PRICE,
  INITIAL_BASE_SIZE,
  QuestionsSection,
} from '@/src/components/statistics/QuestionsSection';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { QUESTIONS_SECTION_TEST_ID } from '@/src/utils/testids';

const mockedProps = {
  sizeFilterOptions: ['s', 'm', 'l', 'xl', 'xxl', '32'],
  initialBrandWithMostProductLt: ['REVIEW'],
  initialBrandWithMostSize: ['Mariposa', 'Laona'],
  initialBrandWithLowestAveragePriceForSize: ['Jake*s Collection'],
};

describe('QuestionsSection.test.tsx', () => {
  afterEach(() => {
    jest.useRealTimers();
  });

  it('should ', () => {
    render(<QuestionsSection {...mockedProps} />);
    expect('1').toBe('1');
  });

  it('should render totally 3 questions', () => {
    render(<QuestionsSection {...mockedProps} />);
    const questionElements = screen.getAllByTestId(
      QUESTIONS_SECTION_TEST_ID.QUESTION,
    );
    expect(questionElements.length).toBe(3);
  });

  it(`should initially render price input with "${INITIAL_BASE_PRICE}" value`, () => {
    render(<QuestionsSection {...mockedProps} />);
    const brandWithMostProductLtInput = screen.getByTestId(
      QUESTIONS_SECTION_TEST_ID.BRAND_WITH_MOST_PRODUCT_LT_INPUT,
    );
    expect(brandWithMostProductLtInput).toHaveValue(INITIAL_BASE_PRICE);
  });

  it(`should initially render initialBrandWithMostProductLt prop values`, () => {
    render(<QuestionsSection {...mockedProps} />);
    const brandWithMostProductLt = screen.getByTestId(
      QUESTIONS_SECTION_TEST_ID.BRAND_WITH_MOST_PRODUCT_LT,
    );
    mockedProps.initialBrandWithMostProductLt.forEach((brand) => {
      expect(brandWithMostProductLt).toContainHTML(brand);
    });
  });
  it('should render brand that offers the largest selection of sizes to the customer', () => {
    render(<QuestionsSection {...mockedProps} />);
    const brandWithMostSize = screen.getByTestId(
      QUESTIONS_SECTION_TEST_ID.BRAND_WITH_MOST_SIZE,
    );
    mockedProps.initialBrandWithMostSize.forEach((brand) => {
      expect(brandWithMostSize).toContainHTML(brand);
    });
  });
  it(`should initially render size input with "${INITIAL_BASE_SIZE}" value`, () => {
    render(<QuestionsSection {...mockedProps} />);
    const brandWithLowestAveragePriceForSizeInput = screen.getByTestId(
      QUESTIONS_SECTION_TEST_ID.BRAND_WITH_LOWEST_AVERAGE_PRICE_INPUT,
    );
    expect(brandWithLowestAveragePriceForSizeInput).toHaveValue(
      INITIAL_BASE_SIZE,
    );
  });
  it('should initially render the "initialBrandWithLowestAveragePriceForSize" prop values', () => {
    render(<QuestionsSection {...mockedProps} />);
    const brandWithLowestAveragePriceForSize = screen.getByTestId(
      QUESTIONS_SECTION_TEST_ID.BRAND_WITH_LOWEST_AVERAGE_PRICE,
    );
    mockedProps.initialBrandWithLowestAveragePriceForSize.forEach((brand) => {
      expect(brandWithLowestAveragePriceForSize).toContainHTML(brand);
    });
  });
  it('should render "-" if there is no brand exist in result', () => {
    render(<QuestionsSection {...mockedProps} initialBrandWithMostSize={[]} />);
    const brandWithMostSize = screen.getByTestId(
      QUESTIONS_SECTION_TEST_ID.BRAND_WITH_MOST_SIZE,
    );
    expect(brandWithMostSize).toContainHTML('-');
  });
  it('should fetch new Brands when the "size" input changed', async () => {
    const request = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ brands: ['REVIEW'] }),
      }),
    ) as jest.Mock;
    global.fetch = request;
    render(<QuestionsSection {...mockedProps} />);
    const input = screen.getByTestId(
      QUESTIONS_SECTION_TEST_ID.BRAND_WITH_LOWEST_AVERAGE_PRICE_INPUT,
    );

    fireEvent.change(input, { target: { value: '44' } });

    await waitFor(() => {
      expect(request).toBeCalled();
    });
  });

  it('should fetch new Brands when the "price" input changed', async () => {
    jest.useFakeTimers();
    const request = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ brands: ['REVIEW'] }),
      }),
    ) as jest.Mock;
    global.fetch = request;
    render(<QuestionsSection {...mockedProps} />);
    const input = screen.getByTestId(
      QUESTIONS_SECTION_TEST_ID.BRAND_WITH_MOST_PRODUCT_LT_INPUT,
    );

    // eslint-disable-next-line testing-library/no-unnecessary-act
    act(() => {
      fireEvent.change(input, { target: { value: 500 } });
      jest.advanceTimersByTime(500);
    });

    await waitFor(() => {
      expect(request).toBeCalled();
    });
  });
});
