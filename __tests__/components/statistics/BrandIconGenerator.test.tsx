import { render, screen } from '@testing-library/react';
import { BrandIconGenerator } from '@/src/components/statistics/BrandIconGenerator';
import { BRAND_ICON_TEST_ID } from '@/src/utils/testids';

describe('BrandIconGenerator.tsx', () => {
  it('should concat first two letters of given multi-word brand name', () => {
    render(<BrandIconGenerator brandName="Blonde No. 8" />);
    const brandAbbreviation = screen.getByTestId(
      BRAND_ICON_TEST_ID.ABBREVIATION,
    );
    expect(brandAbbreviation).toContainHTML('BN');
  });

  it('should just return first letter of single-word brand name', () => {
    render(<BrandIconGenerator brandName="Niente" />);
    const brandAbbreviation = screen.getByTestId(
      BRAND_ICON_TEST_ID.ABBREVIATION,
    );
    expect(brandAbbreviation).toContainHTML('N');
  });

  it('should be rendered as "red" background and "white" text color', () => {
    render(<BrandIconGenerator brandName="Niente" />);
    const brandAbbreviation = screen.getByTestId(
      BRAND_ICON_TEST_ID.ABBREVIATION,
    );
    expect(brandAbbreviation).toHaveClass('bg-red text-white');
  });

  it('should be rendered as 1.75 x 1.75 rem dimensions', () => {
    render(<BrandIconGenerator brandName="Niente" />);
    const brandAbbreviation = screen.getByTestId(
      BRAND_ICON_TEST_ID.ABBREVIATION,
    );
    expect(brandAbbreviation).toHaveClass('w-7 h-7');
  });
});
