import { render, screen } from '@testing-library/react';
import { ProductCard } from '@/src/components/productList/ProductCard';
import { PRODUCT_CARD_TEST_ID } from '@/src/utils/testids';

describe('ProductCard.tsx', () => {
  const mockedProductData = {
    id: '9469349',
    brand: 'REVIEW',
    description: 'Kleid im Double-Layer-Look',
    priceO: 39.95,
    priceR: 19.95,
    url: 'https://www.fashionid.de/review/damen-kleid-im-double-layer-look-fuchsia-9469349_10/',
    images: [
      'https://s3-eu-west-1.amazonaws.com/fid-media-prod/92f90222-b2a4-4522-87e7-6376323d5274.jpg',
      'https://s3-eu-west-1.amazonaws.com/fid-media-prod/f5532672-f6c2-4ae4-88da-1ef0d92b6d6b.png',
    ],
    sizes: ['XS', 'S', 'M', 'L', 'XL'],
  };

  it('should render first image of given product', () => {
    render(<ProductCard productData={mockedProductData} />);
    const productImgElement = screen.getByTestId(
      PRODUCT_CARD_TEST_ID.PRODUCT_IMAGE,
    );
    expect(
      productImgElement
        .getAttribute('src')
        ?.includes(encodeURIComponent(mockedProductData.images[0])),
    ).toBe(true);
  });

  it('should render a "bold" brand text for the name of given product', () => {
    render(<ProductCard productData={mockedProductData} />);
    const productNameElement = screen.getByTestId(
      PRODUCT_CARD_TEST_ID.BRAND_NAME,
    );
    expect(productNameElement).toContainHTML(mockedProductData.brand);
    expect(productNameElement).toHaveClass('font-extrabold');
  });

  it('should render description text of given product', () => {
    render(<ProductCard productData={mockedProductData} />);
    const productDescElement = screen.getByTestId(
      PRODUCT_CARD_TEST_ID.BRAND_DESC,
    );
    expect(productDescElement).toContainHTML(mockedProductData.description);
  });
  it('should render a single "bold" price text if the product has only original price', () => {
    render(
      <ProductCard productData={{ ...mockedProductData, priceR: undefined }} />,
    );
    const priceElement = screen.getByTestId(
      PRODUCT_CARD_TEST_ID.ORIGINAL_PRICE,
    );
    expect(priceElement).toContainHTML(mockedProductData.priceO.toString());
    expect(priceElement).toHaveClass('font-bold');
  });
  it('should render multiple price texts if the product reduced price', () => {
    render(<ProductCard productData={mockedProductData} />);
    const reducedPriceElement = screen.getByTestId(
      PRODUCT_CARD_TEST_ID.REDUCED_PRICE,
    );
    const originalPriceElement = screen.getByTestId(
      PRODUCT_CARD_TEST_ID.ORIGINAL_PRICE,
    );

    expect(reducedPriceElement).toBeInTheDocument();
    expect(originalPriceElement).toBeInTheDocument();
  });
  it('should render original price as "strikethrough" text if it has multiple price texts', () => {
    render(<ProductCard productData={mockedProductData} />);
    const originalPriceElement = screen.getByTestId(
      PRODUCT_CARD_TEST_ID.ORIGINAL_PRICE,
    );
    expect(originalPriceElement).toHaveClass('line-through');
  });
  it('should render reduced price as "bold red" text if it has multiple price texts', () => {
    render(<ProductCard productData={mockedProductData} />);
    const reducedPriceElement = screen.getByTestId(
      PRODUCT_CARD_TEST_ID.REDUCED_PRICE,
    );
    expect(reducedPriceElement).toHaveClass('text-red font-bold');
  });
  it('should has an anchor tag to product detail page', () => {
    render(<ProductCard productData={mockedProductData} />);
    const productCardElement = screen.getByTestId(
      PRODUCT_CARD_TEST_ID.PRODUCT_CARD_WRAPPER,
    );
    expect(productCardElement).toHaveAttribute('href', mockedProductData.url);
  });
});
