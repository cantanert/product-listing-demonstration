import { render, screen, fireEvent } from '@testing-library/react';
import { FilterAndSortingRenderer } from '@/src/components/productList/FilterAndSortingRenderer';
import { FILTER_AND_SORTING_TEST_ID } from '@/src/utils/testids';

describe('FilterAndSortingRenderer.tsx', () => {
  const mockedProps = {
    title: 'Filters',
    data: ['s', 'm', 'l', 'xl'],
    appliedItems: ['s', 'm'],
    clickHandler: jest.fn(),
  };

  it('should render given title', () => {
    render(<FilterAndSortingRenderer {...mockedProps} />);
    const titleElement = screen.getByTestId(FILTER_AND_SORTING_TEST_ID.TITLE);
    expect(titleElement).toContainHTML(mockedProps.title);
  });

  it('should render filter items as much as given dataset count', () => {
    render(<FilterAndSortingRenderer {...mockedProps} />);
    const filterItemElements = screen.getAllByTestId(
      FILTER_AND_SORTING_TEST_ID.FILTER_ITEM,
    );
    expect(filterItemElements.length).toBe(mockedProps.data.length);
  });

  it("should render unapplied filter items as 'gray' background", () => {
    render(<FilterAndSortingRenderer {...mockedProps} />);
    const filterItemElements = screen.getAllByTestId(
      FILTER_AND_SORTING_TEST_ID.FILTER_ITEM,
    );
    filterItemElements.forEach((filterItem) => {
      if (
        mockedProps.data
          .filter((item) => !mockedProps.appliedItems.includes(item))
          .includes(filterItem.innerHTML)
      ) {
        expect(filterItem).toHaveClass('bg-gray-100');
      }
    });
  });

  it("should render applied filter items as 'black' background and 'white' text color", () => {
    render(<FilterAndSortingRenderer {...mockedProps} />);
    const filterItemElements = screen.getAllByTestId(
      FILTER_AND_SORTING_TEST_ID.FILTER_ITEM,
    );
    filterItemElements.forEach((filterItem) => {
      if (mockedProps.appliedItems.includes(filterItem.innerHTML)) {
        expect(filterItem).toHaveClass('bg-black text-white');
      }
    });
  });

  it("should trigger given 'clickHandler' function when the filter item clicked ", () => {
    render(<FilterAndSortingRenderer {...mockedProps} />);
    const filterItemElement = screen.getAllByTestId(
      FILTER_AND_SORTING_TEST_ID.FILTER_ITEM,
    );
    fireEvent.click(filterItemElement[0]);
    expect(mockedProps.clickHandler).toHaveBeenCalledTimes(1);
  });
});
