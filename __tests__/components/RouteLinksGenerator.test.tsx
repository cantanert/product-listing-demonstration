import { render, screen } from '@testing-library/react';
import { ROUTES } from '@/src/utils/routes';
import { RouteLinksGenerator } from '@/src/components/RouteLinksGenerator';
import { usePathname } from 'next/navigation';

const mockUsePathname = jest.fn();

jest.mock('next/navigation', () => ({
  usePathname() {
    return mockUsePathname();
  },
}));

describe('RouteLinksGenerator.tsx', () => {
  it('should render links for all routes array items', () => {
    render(<RouteLinksGenerator />);
    ROUTES.forEach((route) => {
      const link = screen.getByTestId(`${route.name}-route-link`);
      expect(link).toBeInTheDocument();
    });
  });

  it('should render "extrabold and italic" text if the pathname matches with current path', () => {
    render(<RouteLinksGenerator />);
    mockUsePathname.mockImplementation(() => '/Products');
    ROUTES.forEach((route) => {
      if (route.path === usePathname()) {
        const link = screen.getByTestId(`${route.name}-route-link`);
        expect(link).toHaveClass('font-extrabold italic');
      }
    });
  });

  it("should render 'extralight' text if the pathname doesn't matches with current path", () => {
    render(<RouteLinksGenerator />);
    mockUsePathname.mockImplementation(() => '/Products');
    ROUTES.forEach((route) => {
      if (route.path !== usePathname()) {
        const link = screen.getByTestId(`${route.name}-route-link`);
        expect(link).toHaveClass('font-extralight');
      }
    });
  });
});
