import { fetchStatisticsByQuestions } from '@/src/requests/fetchStatisticsByQuestions';
import { EStatisticsUrlParameters } from '@/src/utils/constants';

const mockResponse = { brands: ['Jake*s Casual', 'Lacoste', 'REVIEW'] };

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(mockResponse),
  }),
) as jest.Mock;

describe('fetchStatisticsByQuestions.ts', () => {
  it('should return', async () => {
    const brandStatistics = await fetchStatisticsByQuestions(
      EStatisticsUrlParameters.WITH_MOST_SIZE_OPTIONS,
      'true',
    );
    expect(brandStatistics).toBe(mockResponse);
  });

  it('returns null when exception', async () => {
    global.fetch = jest.fn(() => Promise.reject(null));
    const brandStatistics = await fetchStatisticsByQuestions(
      EStatisticsUrlParameters.WITH_MOST_SIZE_OPTIONS,
      'true',
    );
    expect(brandStatistics).toBe(null);
  });
});
