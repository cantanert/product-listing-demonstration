
## Features

- **Eslint** with **Airbnb** ruleset for code quality
- **Prettier** for code formatting
- **Typescript** for more type-safety and predictible codebase
- **Tailwind** util css classes.
- **Husky** for managing git hooks
- **commitlint, standart-version and Conventional Commits** generating automated changelogs from git commit history, establishing commit convention and semantic versioning.
- **Jest** and **React Testing Library** for unit testing

### Expandable Features 
- **React Context** could be implemented instead of prop drilling.
- **React Query** will be a good choice for managing server states for possible further external API's.
- Internationalization
- Comprehensive error handling with empowered UI&UX
- Comprehensive SEO improvements on production.


## API Reference

#### Get products

```http
  GET /api/search
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `sortBy` | `string` | Sort criteria for sorting. ["PRICE_DESC", "PRICE_DESC"] |
| `size` | `string` | Size specification for filtering |

```http
  GET /api/search?sortBy=PRICE_DESC&size=5XL
```

#### Get product statistics

```http
  GET /api/statistics
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `withMostProductLtPrice`      | `string` | Price specification to find the brand with products that cheaper than the given amount  |
| `withMostSizeOptions`      | `-` | Parameter specifitaion to find the brand with products that with most size options  |
| `withLowestAveragePriceForSize`      | `string` | Size specification to find the brand with products that with lowest average   |

```http
  GET /api/statistics?withMostProductLtPrice=40
```
```http
  GET /api/statistics?withMostSizeOptions
```
```http
  GET /api/statistics?withLowestAveragePriceForSize=32
```


## Color Reference

| Color             | Hex                                                                |
| ----------------- | ------------------------------------------------------------------ |
| Black | ![#0a192f](https://via.placeholder.com/10/000?text=+) #000 |
| White | ![#f8f8f8](https://via.placeholder.com/10/fff?text=+) #fff |
| Red | ![#00d1a0](https://via.placeholder.com/10/ff0303?text=+) #00d1a0 |


## Run Locally

Clone the project

```bash
  git clone https://github.com/cantanert/product-listing-demonstration
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  npm i
```

Start the server

```bash
  npm run dev
```
### Important Notes

- Development server needs to be started in **3000** port of localhost. If you want to run app locally in another port, you should change `BASE_URL` constant in **urls.ts**
  http://localhost:3000/
- **Node** version have to be **18** or higher.


## Running Tests

To run tests, run the following command

```bash
  npm run test
```

It is better to use "watch" command while developing
```bash
  npm run test:watch
```

## Semantic Versioning

Minor Version
```bash
npm run release:minor
```
Major Version
```bash
npm run release:major
```
Patch Version
```bash
npm run release:patch
```

## Feedback

I really care about both positive and negative feedbacks. So if you have any feedback, I would be very happy if you reach out to me at cantanert@gmail.com

