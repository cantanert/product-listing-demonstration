import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import React from 'react';
import { Navbar } from '@/src/components/Navbar';
import classNames from 'classnames';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'LOREMIPSUM',
  description: 'Lorem ipsum dolor sit amet',
  openGraph: {
    title: 'LOREMIPSUM',
    description: 'Lorem ipsum dolor sit amet',
    url: '[PRODUCTION_URL]',
    siteName: 'LOREMIPSUM',
    images: [
      {
        url: '[CDN_URL]',
        width: 800,
        height: 600,
      },
      {
        url: '[CDN_URL]',
        width: 1800,
        height: 1600,
        alt: 'alt',
      },
    ],
    locale: 'en_US',
    type: 'website',
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body
        className={classNames({
          [inter.className]: true,
          'flex flex-col justify-center items-center': true,
        })}
      >
        <Navbar />
        <div className="max-w-screen-widescreen w-full pb-10 pl-2 pr-2 widescreen:pl-0 widescreen:pr-0">
          {children}
        </div>
      </body>
    </html>
  );
}
