import { fetchProducts } from '@/src/requests/fetchProducts';
import { ProductCard } from '@/src/components/productList/ProductCard';
import { IProduct } from '@/src/models/product';
import { SizeFilter } from '@/src/components/productList/SizeFilter';
import { SortingByPrice } from '@/src/components/productList/SortingByPrice';
import { ESearchUrlParameters } from '@/src/utils/constants';
import { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Products | LOREMIPSUM',
};

const ProductListPage = async ({ searchParams }: { searchParams: any }) => {
  const searchParameters = new URLSearchParams();

  if (searchParams[ESearchUrlParameters.SIZE]) {
    searchParameters.set(
      ESearchUrlParameters.SIZE,
      searchParams[ESearchUrlParameters.SIZE],
    );
  }

  if (searchParams[ESearchUrlParameters.SORT_BY]) {
    searchParameters.set(
      ESearchUrlParameters.SORT_BY,
      searchParams[ESearchUrlParameters.SORT_BY],
    );
  }

  const products = await fetchProducts(searchParameters.toString());

  return (
    <div id="ProductListPage">
      <p className="pb-5">
        Total <span className="font-bold">{products?.page?.totalCount}</span>{' '}
        product
        {`${products?.page?.totalCount > 1 ? 's' : ''}`}
      </p>
      {products?.page?.totalCount ? (
        <div className="flex flex-col desktop:flex-row gap-5 desktop:gap-0">
          <SizeFilter data={products?.sizeFilterOptions ?? []} />
          <div className="flex flex-1 order-3 desktop:order-2 flex-wrap">
            {products?.results.map((product: IProduct) => (
              <ProductCard
                className="w-1/2 tablet:w-1/3 desktop:w-1/4"
                key={product.id}
                productData={product}
              />
            ))}
          </div>
          <SortingByPrice data={products?.sortingOptions ?? []} />
        </div>
      ) : (
        <p>There is no product exist...</p>
      )}
    </div>
  );
};

export default ProductListPage;
