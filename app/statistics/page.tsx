import { fetchProducts } from '@/src/requests/fetchProducts';
import { QuestionsSection } from '@/src/components/statistics/QuestionsSection';
import { fetchStatisticsByQuestions } from '@/src/requests/fetchStatisticsByQuestions';
import { EStatisticsUrlParameters } from '@/src/utils/constants';
import { Metadata } from 'next';

const DEFAULT_SIZE = '32';
const DEFAULT_PRICE = '40';

export const metadata: Metadata = {
  title: 'Statistics | LOREMIPSUM',
};

const StatisticsPage = async () => {
  const [
    brandWithMostProductLt,
    brandWithMostSize,
    brandWithLowestAveragePriceForSize,
    products,
  ] = await Promise.all([
    fetchStatisticsByQuestions(
      EStatisticsUrlParameters.WITH_MOST_PRODUCT_LT_PRICE,
      DEFAULT_PRICE,
    ),
    fetchStatisticsByQuestions(EStatisticsUrlParameters.WITH_MOST_SIZE_OPTIONS),
    fetchStatisticsByQuestions(
      EStatisticsUrlParameters.LOWEST_AVERAGE_PRICE_FOR_SIZE,
      DEFAULT_SIZE,
    ),
    fetchProducts(),
  ]);

  const { sizeFilterOptions } = products;

  return (
    <div id="StatisticsPage">
      <QuestionsSection
        sizeFilterOptions={sizeFilterOptions}
        initialBrandWithMostProductLt={brandWithMostProductLt?.brands}
        initialBrandWithMostSize={brandWithMostSize?.brands}
        initialBrandWithLowestAveragePriceForSize={
          brandWithLowestAveragePriceForSize?.brands
        }
      />
    </div>
  );
};

export default StatisticsPage;
