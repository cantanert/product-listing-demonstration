import { RouteLinksGenerator } from '@/src/components/RouteLinksGenerator';

export default function Home() {
  return (
    <div id="Homepage">
      <h1 className="font-extrabold">Please click a route for navigation:</h1>
      <div className="flex flex-col gap-2 pt-5 ">
        <RouteLinksGenerator />
      </div>
    </div>
  );
}
