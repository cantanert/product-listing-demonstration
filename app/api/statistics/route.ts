import { EStatus } from '@/src/utils/constants';
import { NextRequest, NextResponse } from 'next/server';
import { MESSAGE } from '@/src/utils/messages';
import { fetchProductsFromDataSource } from '@/src/requests/fetchProductsFromDataSource';
import {
  findTheBrand,
  statisticResponseObjectGenerator,
} from '@/app/api/statistics/statistics';

export const GET = async (req: NextRequest) => {
  try {
    const products = await fetchProductsFromDataSource();

    const { searchParams } = req.nextUrl;

    const brand = findTheBrand(products, searchParams);

    return NextResponse.json(statisticResponseObjectGenerator(brand));
  } catch (e: any) {
    if (e.statusCode) {
      return new NextResponse(e, {
        status: e.statusCode,
      });
    }

    const error = {
      status: EStatus.ERROR,
      message: e?.message ?? MESSAGE.INTERNAL_SERVER_ERROR,
    };

    return new NextResponse(JSON.stringify(error), {
      status: 500,
    });
  }
};
