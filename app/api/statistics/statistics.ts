import { IProduct } from '@/src/models/product';
import {
  EInterpolationString,
  EStatisticsUrlParameters,
} from '@/src/utils/constants';
import { MESSAGE } from '@/src/utils/messages';
import { groupByProperty } from '@/src/utils/functions';
import { CustomError } from '@/src/utils/classes';

export const statisticResponseObjectGenerator = (brands: string[]) => ({
  brands,
});

const findTheBrandWithMostSizeOptions = (products: IProduct[]) => {
  let maxSizeCount = 0;
  let resultBrands: string[] = [];
  const brandGroupedProducts = groupByProperty(products, 'brand');
  Object.keys(brandGroupedProducts).forEach((brand: string) => {
    const brandMaxSizeCount = Math.max(
      ...brandGroupedProducts[brand].map((product) => product.sizes.length),
    );
    if (brandMaxSizeCount === maxSizeCount) {
      resultBrands.push(brand);
    } else if (brandMaxSizeCount > maxSizeCount) {
      maxSizeCount = brandMaxSizeCount;
      resultBrands = [brand];
    }
  });

  return resultBrands;
};

const findTheBrandWithLowestAveragePrice = (
  products: IProduct[],
  size: string,
) => {
  const filteredProductsWithSize = products.filter((product) =>
    product.sizes.includes(size.toUpperCase()),
  );

  if (!filteredProductsWithSize.length) return [''];

  const brandGroupedProducts = groupByProperty(
    filteredProductsWithSize,
    'brand',
  );

  if (Object.keys(brandGroupedProducts).length === 1) {
    const brand = Object.keys(brandGroupedProducts)[0];
    return [brand];
  }

  let lowestAveragePrice = 0;
  let resultBrands: string[] = [];

  Object.keys(brandGroupedProducts).forEach((brand) => {
    const brandProductPrices: number[] = [];
    brandGroupedProducts[brand].forEach((product) => {
      brandProductPrices.push(product.priceR ?? product.priceO);
    });
    const brandAverageProductPrice =
      brandProductPrices.reduce((acc, cur) => acc + cur, 0) /
      brandProductPrices.length;

    if (
      lowestAveragePrice === 0 ||
      brandAverageProductPrice < lowestAveragePrice
    ) {
      lowestAveragePrice = brandAverageProductPrice;
      resultBrands = [brand];
    } else if (brandAverageProductPrice === lowestAveragePrice) {
      resultBrands.push(brand);
    }
  });

  return resultBrands;
};

const findTheBrandWithMostProduct = (products: IProduct[], price: string) => {
  const filteredProductsWithPrice = products.filter((product) => {
    const productPrice = product.priceR ?? product.priceO;
    return productPrice < +price;
  });

  if (!filteredProductsWithPrice.length) return [''];

  const brandGroupedProducts = groupByProperty(
    filteredProductsWithPrice,
    'brand',
  );

  if (Object.keys(brandGroupedProducts).length === 1) {
    const brand = Object.keys(brandGroupedProducts)[0];
    return [brand];
  }

  let maxProductCount = 0;
  let resultBrands: string[] = [];

  Object.keys(brandGroupedProducts).forEach((brand) => {
    const brandsProductsCount = brandGroupedProducts[brand].length;

    if (brandsProductsCount === maxProductCount) {
      resultBrands.push(brand);
    } else if (brandsProductsCount > maxProductCount) {
      resultBrands = [brand];
      maxProductCount = brandsProductsCount;
    }
  });

  return resultBrands;
};

export const findTheBrand = (
  products: IProduct[],
  searchParams: URLSearchParams,
): string[] => {
  const WITH_MOST_SIZE_OPTIONS = searchParams.get(
    EStatisticsUrlParameters.WITH_MOST_SIZE_OPTIONS,
  );
  const LOWEST_AVERAGE_PRICE_FOR_SIZE = searchParams.get(
    EStatisticsUrlParameters.LOWEST_AVERAGE_PRICE_FOR_SIZE,
  );
  const WITH_MOST_PRODUCT_LT = searchParams.get(
    EStatisticsUrlParameters.WITH_MOST_PRODUCT_LT_PRICE,
  );

  if (
    WITH_MOST_SIZE_OPTIONS === null &&
    LOWEST_AVERAGE_PRICE_FOR_SIZE === null &&
    WITH_MOST_PRODUCT_LT === null
  ) {
    throw new CustomError(
      MESSAGE.UNSPECIFIED_PARAMETER.replace(
        EInterpolationString.PARAMETER,
        `${Object.values(EStatisticsUrlParameters)}`,
      ),
      400,
    );
  }

  if (WITH_MOST_SIZE_OPTIONS !== null) {
    return findTheBrandWithMostSizeOptions(products);
  }

  if (LOWEST_AVERAGE_PRICE_FOR_SIZE === '') {
    throw new CustomError(
      MESSAGE.UNDEFINED_PARAMETER_VALUE.replace(
        EInterpolationString.PARAMETER,
        EStatisticsUrlParameters.LOWEST_AVERAGE_PRICE_FOR_SIZE,
      ),
      400,
    );
  } else if (LOWEST_AVERAGE_PRICE_FOR_SIZE) {
    return findTheBrandWithLowestAveragePrice(
      products,
      LOWEST_AVERAGE_PRICE_FOR_SIZE,
    );
  }

  if (WITH_MOST_PRODUCT_LT === '') {
    throw new CustomError(
      MESSAGE.UNDEFINED_PARAMETER_VALUE.replace(
        EInterpolationString.PARAMETER,
        EStatisticsUrlParameters.WITH_MOST_PRODUCT_LT_PRICE,
      ),
      400,
    );
  } else if (WITH_MOST_PRODUCT_LT) {
    if (Number.isNaN(+WITH_MOST_PRODUCT_LT)) {
      throw new CustomError(
        MESSAGE.INVALID_PARAMETER.replace(
          EInterpolationString.PARAMETER,
          EStatisticsUrlParameters.WITH_MOST_PRODUCT_LT_PRICE,
        ),
        400,
      );
    }

    return findTheBrandWithMostProduct(products, WITH_MOST_PRODUCT_LT);
  }

  throw new CustomError(MESSAGE.INTERNAL_SERVER_ERROR, 500);
};
