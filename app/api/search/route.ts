import { EStatus } from '@/src/utils/constants';
import { NextRequest, NextResponse } from 'next/server';
import { MESSAGE } from '@/src/utils/messages';
import { fetchProductsFromDataSource } from '@/src/requests/fetchProductsFromDataSource';
import {
  applyProductFiltersAndSorting,
  searchResponseObjectGenerator,
} from '@/app/api/search/search';

export const GET = async (req: NextRequest) => {
  try {
    const products = await fetchProductsFromDataSource();
    const { searchParams } = req.nextUrl;

    const preparedSearchResponse = applyProductFiltersAndSorting(
      products,
      searchParams,
    );

    return NextResponse.json(
      searchResponseObjectGenerator(preparedSearchResponse),
    );
  } catch (e: any) {
    const error = {
      status: EStatus.ERROR,
      message: e?.message ?? MESSAGE.INTERNAL_SERVER_ERROR,
    };

    return new NextResponse(JSON.stringify(error), {
      status: 500,
    });
  }
};
