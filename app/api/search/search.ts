import { IExtendedProduct, IProduct } from '@/src/models/product';
import {
  EDelimiter,
  ESearchUrlParameters,
  ESortingOptions,
} from '@/src/utils/constants';

export const SORTING_FUNCTIONS = {
  [ESortingOptions.PRICE_ASC]: (a: IExtendedProduct, b: IExtendedProduct) =>
    a.exactPrice - b.exactPrice,
  [ESortingOptions.PRICE_DESC]: (a: IExtendedProduct, b: IExtendedProduct) =>
    b.exactPrice - a.exactPrice,
};
export const applyProductFiltersAndSorting = (
  products: IProduct[],
  searchParams: URLSearchParams,
) => {
  const SORT_BY = searchParams.get(ESearchUrlParameters.SORT_BY);
  const SIZE = searchParams.get(ESearchUrlParameters.SIZE);

  const allAvailableSizes: string[] = [];

  let modifiedProducts: IExtendedProduct[] = products.map((product) => {
    product.sizes.forEach((size) => {
      if (!allAvailableSizes.includes(size)) allAvailableSizes.push(size);
    });
    return {
      ...product,
      exactPrice: product?.priceR ?? product.priceO,
    };
  });

  if (SIZE) {
    const targetSizes = SIZE.toUpperCase().split(EDelimiter.COMMA);
    modifiedProducts = modifiedProducts.filter((product) => {
      for (const productSize of product.sizes) {
        if (targetSizes.includes(productSize)) {
          return true;
        }
      }
      return false;
    });
  }

  if (SORT_BY && Object.keys(ESortingOptions).includes(SORT_BY))
    modifiedProducts.sort(SORTING_FUNCTIONS[SORT_BY as ESortingOptions]);

  return {
    results: modifiedProducts,
    allAvailableSizes,
  };
};

export const searchResponseObjectGenerator = ({
  results,
  allAvailableSizes,
}: {
  results: IProduct[];
  allAvailableSizes: string[];
}) => ({
  results,
  sizeFilterOptions: allAvailableSizes,
  sortingOptions: Object.keys(ESortingOptions),
  page: {
    totalCount: results.length,
    // TODO: pagination
  },
});
